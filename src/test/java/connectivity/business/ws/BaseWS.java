package connectivity.business.ws;

import common.utils.Xml;
import connectivity.utils.EnvConfigs;
import java.util.HashMap;
import java.util.Map;

public abstract class BaseWS {
    protected Map<String, String> commonModMap;
    protected Xml request;
    protected Xml response;

    protected BaseWS() {
        this.commonModMap = new HashMap<>();
    }
}
