package connectivity.business.ws.ews;

import common.utils.Random;
import common.utils.Xml;
import connectivity.business.ws.BaseWS;
import java.io.File;
import java.util.Map;

public class EWSActions extends BaseWS {
    //region XML files
    public static final String TOUR_LIST = "src/test/resources/schema/xml/TourList.xml";
    public static final String AVAILABILITY = "src/test/resources/schema/xml/Availability.xml";
    public static final String BATCH_AVAILABILITY = "src/test/resources/schema/xml/BatchAvailability.xml";
    public static final String BOOKING = "src/test/resources/schema/xml/Booking.xml";
    public static final String BATCH_PRICING = "src/test/resources/schema/xml/BatchPricing.xml";
    //endregion

    public EWSActions() {
        super();
        putCommonModMap();
    }

    private void putCommonModMap() {
        String uniqueRandomString = Random.getRandomAlphaString(10);
        String firstName = "firstName" + uniqueRandomString;
        String lastName = "lastName" + uniqueRandomString;
        this.commonModMap.put("BookingReference", Random.getRandomNumericString(9));
        this.commonModMap.put("GivenName", firstName);
        this.commonModMap.put("Surname", lastName);
        this.commonModMap.put("ContactName", firstName + " " + lastName);
    }

    public String createTourListRequestBySupplierId(String supplierId) {
        request = new Xml(new File(TOUR_LIST));
        request.setTextByTagName("SupplierId", supplierId);
        return request.toString();
    }

    public String createBatchAvailabilityRequest(Map<String, String> mods) {
        request = new Xml(new File(BATCH_AVAILABILITY));
        request.setTextByTagName(mods);
        return request.toString();
    }

    public String createAvailabilityRequest(Map<String, String> mods) {
        request = new Xml(new File(AVAILABILITY));
        request.setTextByTagName(mods);
        return request.toString();
    }

    public String createBookingRequest(Map<String, String> mods) {
        request = new Xml(new File(BOOKING));
        request.setTextByTagName(commonModMap);
        request.setTextByTagName(mods);
        return request.toString();
    }

    public String createBatchPricingRequest(Map<String, String> mods) {
        request = new Xml(new File(BATCH_PRICING));
        request.setTextByTagName(mods);
        return request.toString();
    }
}
