package connectivity.pages;

import common.wdm.WdManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import java.time.Duration;


public class BasePage {

    public BasePage() {
        PageFactory.initElements(WdManager.getAjaxEle(), this);
    }

    //region Useful actions

    /**
     * @param tbl table which contains labels and values
     * @param lbl label to fill value in
     * @param val value to be filled
     */
    protected void enterValueByLabel(WebElement tbl, String lbl, String val) {
        WebElement ele =
                tbl.findElement(By.xpath("//tbody//tr//td[div//button//span[contains(.,'" + lbl + "')]]/following-sibling::td/input"));
        ele.sendKeys(val);
    }

    /**
     * @param lbl label to fill value in
     * @param val value to be filled
     */
    protected void enterValueByLabel(String lbl, String val) {
        WebElement ele = WdManager.get().findElement(By.xpath("//label[contains(.,'" + lbl + "')]/following-sibling::div/input"));
        ele.sendKeys(val);
    }

    protected void enterValue(WebElement element, String val) {
        element.clear();
        element.sendKeys(val);
    }

    protected WebElement getCell(WebElement tbl, int row, int col) {
        return tbl.findElement(By.xpath("//tbody//tr[" + row + "]/td[" + col + "]"));
    }

    protected void waitUntilElementClickable(WebElement ele) {
        WdManager.getWait()
                .withTimeout(Duration.ofSeconds(30))
                .pollingEvery(Duration.ofSeconds(30))
                .ignoring(Throwable.class)
                .until(ExpectedConditions.elementToBeClickable(ele));
    }

    protected void waitUntilElementInvisible(WebElement ele) {
        WdManager.getWait().until(ExpectedConditions.invisibilityOf(ele));
    }

    protected boolean isDisplayed(WebElement ele) {
        try {
            return ele.isDisplayed();
        } catch (Throwable ex) {
            return false;
        }
    }

    protected void click(WebElement elm) {
        elm.click();
    }

    public void scrollToElement(WebElement element) {
        ((JavascriptExecutor) WdManager.get()).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    protected void waitUntilProgressbarLoaded() {
        int timeOut  = 1330000;
        try {
            while (isDisplayed(WdManager.get().findElement(By.xpath("//*[@class='peg' or @class='kbnLoaderWrap']"))) && timeOut > 0) {
                timeOut -= 1000;
                Thread.sleep(2000);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    protected void selectByVisibleText(WebElement element, String value) {
        Select drpCountry = new Select(element);
        drpCountry.selectByVisibleText(value);
    }

    protected void selectByIndex(WebElement element, int index) {
        Select drpCountry = new Select(element);
        drpCountry.selectByIndex(index);
    }

    protected void selectByValue(WebElement element, String value) {
        Select drpCountry = new Select(element);
        drpCountry.selectByValue(value);
    }

    protected String getText(WebElement element) {
        return element.getText();
    }

    protected String getValue(WebElement element) {
        return element.getAttribute("value");
    }
    //endregion
}
