package connectivity.pages.tgdslog;

import common.wdm.WdManager;
import connectivity.pages.BasePage;
import connectivity.pages.TableBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RequestLogPage extends BasePage {
    @FindBy(id = "supplier_selector")
    WebElement ddSupplier;

    @FindBy(className = "checkbox")
    WebElement apiType;

    @FindBy(xpath = "//button[.='Submit']")
    WebElement btnSubmit;

    @FindBy(id = "logTable")
    WebElement tblLog;


    TableBase tableBase;
    public void openTgdsRequestLog(String url){
        WdManager.get().get(url);
    }

    public void selectSupplier(String supplierId){
        selectByVisibleText(ddSupplier, supplierId);
        waitUntilProgressbarLoaded();
    }

    public void selectApiType(String searchText){
        WebElement elm = apiType.findElement(By.xpath(".//input[@name='"+searchText+"']"));
        if (!elm.isSelected()){
            click(elm);
        }
    }

    public void clickSubmitBtn(){
        click(btnSubmit);
        waitUntilProgressbarLoaded();
    }

    public void openRequestLogByIndex(int index) {
        tableBase = new TableBase(tblLog);
        WebElement firstLink = tableBase.findLinksByText("Details").get(index - 1);
        click(firstLink);
    }
}
