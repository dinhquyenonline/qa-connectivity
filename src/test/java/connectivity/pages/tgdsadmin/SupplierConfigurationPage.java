package connectivity.pages.tgdsadmin;

import connectivity.pages.BasePage;
import connectivity.pages.TableBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.Map;

public class SupplierConfigurationPage extends BasePage {
    @FindBy(xpath = "//button[contains(@class,'edit-mode')]")
    WebElement btnEditMode;

    @FindBy(id = "saveNewSupplierBtn")
    WebElement btnSave;

    @FindBy(xpath = "//div[@id='supplierConfigs']//table")
    WebElement tblSupplierConfigs;
    TableBase tableBase = new TableBase(tblSupplierConfigs);

    public void clickEditBtn(){
        click(btnEditMode);
        waitUntilProgressbarLoaded();
    }

    public void changeSupplierConfigurations(Map<String, String> configs){
        configs.forEach((k, v) -> {
           if (isDisplayed(tableBase.findElementByCellValueAndRowClassName(k, "configName edit"))){
               WebElement configNameElm = tableBase.findElementByCellValueAndRowClassName(k, "configName edit").findElement(By.xpath("./following-sibling::td//input[@ng-model='config.value']"));
               enterValue(configNameElm, v);
           }else {

           }
        });
    }
}
