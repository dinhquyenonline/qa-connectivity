package connectivity.pages.tgdsadmin;

import connectivity.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.AbstractMap;

public class TgdsAdminHomePage extends BasePage {

    @FindBy(xpath = "//div[@id='suppliers-link']/following-sibling::div[@class='panel-body']")
    WebElement suppliersSection;

    @FindBy(id = "search-supplier-text")
    WebElement txtSearchTextSupplier;

    @FindBy(id = "search-button")
    WebElement btnSearch;

    @FindBy(xpath = "//table[contains(@class,'supplier-table')]")
    WebElement tblSupplier;

    @FindBy(xpath = "//tbody/tr[contains(@class,'supplier-table-row')]")
    WebElement firstRow;

    @FindBy(xpath = "//button[contains(@class,'edit-mode')]")
    WebElement btnEditMode;

    public boolean isHomePageDisplayed(){
        return isDisplayed(suppliersSection);
    }

    public void findSupplier(AbstractMap.SimpleEntry<String, String> ... pairs) {
        click(suppliersSection);
        waitUntilElementClickable(btnSearch);
        for (AbstractMap.SimpleEntry<String, String> p : pairs) {
            enterValueByLabel(p.getKey(), p.getValue());
        }
        click(btnSearch);
        waitUntilElementClickable(firstRow);
        openSupplierByIndex(1);
        waitUntilElementClickable(btnEditMode);
    }

    public void openSupplierByIndex(int index) {
       click(getCell(tblSupplier, index, 1));
    }
}
