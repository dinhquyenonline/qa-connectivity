package connectivity.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class TableBase extends BasePage {
    WebElement element;

    public TableBase(WebElement element) {
        this.element = element;
    }

    public int getRowsCount() {
        return element.findElements(By.xpath(".//tbody//tr")).size();
    }

    public WebElement findRecordByIndex(int index) {
        return getRowByIndex(index);
    }

    public List<WebElement> findLinksByText(String text) {
        try {
            return element.findElements(By.xpath(String.format(".//tr/td//a[contains(text(),'%s')]", text)));
        } catch (Exception e) {
            return null;
        }
    }

    public String getCellValueByColumnNameAndRowIndex(int index, String columnName) {
        // i = 1: Header
        try {
            WebElement row = getRowByIndex(index);
            int columnIndex = getColumnIndex(columnName);
            String xpath = String.format(".//td[%d]", columnIndex + 1);
            return row.findElement(By.xpath(xpath)).getText();
        } catch (Exception e) {
            return null;
        }
    }

    public String getCellAttributeValueByColumnNameAndRowIndex(int index, String columnName) {
        try {
            WebElement row = getRowByIndex(index);
            int columnIndex = getColumnIndex(columnName);
            String xpath = String.format(".//td[%d]//input", columnIndex + 2);
            return row.findElement(By.xpath(xpath)).getAttribute("value");
        } catch (Exception e) {
            return null;
        }
    }

    public WebElement findElementByColumnNameAndRowIndex(int index, String columnName) {
        try {
            WebElement row = getRowByIndex(index);
            int columnIndex = getColumnIndex(columnName);
            String xpath = String.format(".//td[%d]", columnIndex + 1);
            return row.findElement(By.xpath(xpath));
        } catch (Exception e) {
            return null;
        }
    }

    public WebElement findElementByColumnIndexAndRowIndex(int rowIndex, int columnIndex) {
        try {
            WebElement row = getRowByIndex(rowIndex);
            String xpath = String.format(".//td[%d]", columnIndex + 1);
            return row.findElement(By.xpath(xpath));
        } catch (Exception e) {
            return null;
        }
    }

    public WebElement findElementByRowClassName(String rowClassName) {
        try {
            WebElement row = element.findElement(By.xpath(String.format(".//tbody//tr[contains(@class,'%s')]", rowClassName)));
            return row;
        } catch (Exception e) {
            return null;
        }
    }

    public WebElement findElementByCellValueAndRowClassName(String cellValue, String rowClassName) {
        try {
            return element.findElement(By.xpath(String.format(".//tbody//tr//td[contains(.,'%s') and contains(@class,'%s')]", cellValue, rowClassName)));
        } catch (Exception e) {
            return null;
        }
    }

    public WebElement findElementByCellValue(String cellValue) {
        try {
            return element.findElement(By.xpath(String.format(".//tbody//tr//td[contains(.,'%s')]", cellValue)));
        } catch (Exception e) {
            return null;
        }
    }

    public WebElement findCellByLabel(String label) {
        try {
            String xpath = String.format(".//td[@class='label' and contains(text(),'%s')]//following-sibling::td", label);
            WebElement row = element.findElement(By.xpath(xpath));
            return row;
        } catch (Exception e) {
            return null;
        }
    }

    public WebElement findElementByCellValue(WebElement innerRow, String columnName) {
        int columnIndex = getColumnIndex(columnName);
        return innerRow.findElement(By.xpath(String.format(".//td[%d]", columnIndex + 1)));
    }

    public WebElement findColumnByIndex(int index) {
        List<WebElement> rows = element.findElements(By.tagName("tr"));
        for (WebElement row : rows) {
            List<WebElement> cols = row.findElements(By.xpath("td"));
            for (int i = 1; i <= cols.size(); i++) {
                String str = cols.get(i).getText().toString();
                if (i == index) {
                    return cols.get(index);
                }
            }
        }
        return null;
    }


    public WebElement findColumnsByName(String colName) {
        List<WebElement> rows = element.findElements(By.tagName("tr"));
        for (WebElement row : rows) {
            List<WebElement> cols = row.findElements(By.xpath("td"));
            for (WebElement col : cols) {
                String str = col.getText().toString();
                if (str.equalsIgnoreCase(colName)) {
                    return col;
                }
            }
        }
        return null;
    }


    public WebElement getRowByColumnNameAndCellValue(String columnName, String cellValue) {
        List<WebElement> body = getBody();
        int columnIndex = getColumnIndex(columnName);
        for (WebElement element : body) {
            if (element.findElements(By.tagName("td")).get(columnIndex).getText().equalsIgnoreCase(cellValue)) {
                return element;
            }
        }
        return null;
    }

    public WebElement getRowByContainsColumnNameAndCellValue(String columnName, String cellValue) {
        List<WebElement> body = getBody();
        int columnIndex = getColumnIndex(columnName);
        for (WebElement element : body) {
            if (element.findElements(By.tagName("td")).get(columnIndex).getText().contains(cellValue)) {
                return element;
            }
        }
        return null;
    }

    private List<WebElement> getBody() {
        return element.findElements(By.xpath(".//tr"));
    }

    private int getColumnIndex(String columnName) {
        int columnIndex = 0;
        List<WebElement> header = element.findElements(By.tagName("tr"));
        for (WebElement row : header) {
            List<WebElement> cols = row.findElements(By.xpath("td"));
            for (WebElement col : cols) {
                String str = col.getText();
                if (str.equals(columnName)) {
                    columnIndex = cols.indexOf(col);
                    break;
                }
            }
        }
        return columnIndex;
    }

    private int getColumnIndexTagTh(String columnName) {
        int columnIndex = 0;
        List<WebElement> header = element.findElements(By.tagName("tr"));
        for (WebElement row : header) {
            List<WebElement> cols = row.findElements(By.xpath("th"));
            for (WebElement col : cols) {
                String str = col.getText();
                if (str.equals(columnName)) {
                    columnIndex = cols.indexOf(col);
                    break;
                }
            }
        }
        return columnIndex;
    }

    private WebElement getRowByIndex(int index) {
        String xPath = String.format("./tbody/tr[%d]", index);
        try {
            return element.findElement(By.xpath(xPath));
        } catch (Exception e) {
            return null;
        }
    }

    public List<WebElement> getRowsByColumnsWithIndex(String[] cellValue) {
        List<Boolean> listChecked = new ArrayList<>();
        List<WebElement> list = new ArrayList<>();
        List<WebElement> body = getBody();
        for (WebElement element : body) {
            List<WebElement> td = element.findElements(By.tagName("td"));
            for (int i = 0; i < td.size(); i++) {
                if (element.findElements(By.tagName("td")).get(i).getText().contains(cellValue[i])) {
                    listChecked.add(true);
                } else {
                    listChecked.add(false);
                }
            }
            if (listChecked.stream().filter(x -> x == false).count() < 1) {
                list.add(element);
            }
            listChecked = new ArrayList<>();
        }
        return list;
    }


    public List<WebElement> getAllRows() {
        return element.findElements(By.tagName("tr"));
    }

    public List<List<String>> getAllCellValue() {
        List<List<String>> hashMapList = new ArrayList<>();
        List<WebElement> body = getBody();
        for (int i = 1; i < body.size(); i++) {
            List<String> list = new ArrayList<>();
            List<WebElement> td = body.get(i).findElements(By.tagName("td"));
            for (int y = 0; y < td.size(); y++) {
                list.add(td.get(y).getText());
            }
            hashMapList.add(list);
        }
        return hashMapList;
    }

    public List<WebElement> getAllColumnsByColumnName(String columnName) {
        int columnIndex = 0;
        WebElement elm = null;
        List<WebElement> column = new ArrayList<>();
        List<WebElement> body = getBody();
        columnIndex = getColumnIndex(columnName);
        for (int i = 1; i < body.size(); i++) {
            column.add(body.get(i).findElements(By.tagName("td")).get(columnIndex));
        }
        return column;
    }

    public List<WebElement> getRowsByColumnNameAndCellValue(String columnName, String cellValue) {
        List<WebElement> body = getBody();
        List<WebElement> el = new ArrayList<>();
        int columnIndex = getColumnIndex(columnName);
        for (WebElement element : body) {
            if (element.findElements(By.tagName("td")).get(columnIndex).getText().equalsIgnoreCase(cellValue)) {
                el.add(element);
            }
        }
        return el;
    }

}
