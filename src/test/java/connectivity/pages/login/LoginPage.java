package connectivity.pages.login;

import common.wdm.WdManager;
import connectivity.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {
    @FindAll({@FindBy(id = "username"), @FindBy(name = "email"), @FindBy(name = "username")})
    WebElement txtUsername;

    @FindAll({@FindBy(id = "password"), @FindBy(name = "password"), @FindBy(name = "password")})
    WebElement txtPassword;

    @FindBy(name = "supplierCode")
    WebElement txtSupplierCode;

    @FindAll({@FindBy(id = "login-button"), @FindBy(id = "submit_btn_sl"), @FindBy(xpath = "//button[@data-test-subj='loginSubmit']")})
    WebElement btnOk;

    @FindAll({@FindBy(className = "navbar-brand"), @FindBy(id = "reactRoot"), @FindBy(id = "kibana-body")})
    WebElement navbarBrand;


    public void openLoginPage(String url){
        WdManager.get().get(url);
        waitUntilProgressbarLoaded();
    }

    public void login(String username, String password) {
        txtUsername.sendKeys(username);
        txtPassword.sendKeys(password);
        btnOk.click();
        waitUntilElementClickable(navbarBrand);
    }

    public void login(String username, String password, String supplierCode) {
        txtUsername.sendKeys(username);
        txtPassword.sendKeys(password);
        txtSupplierCode.sendKeys(supplierCode);
        btnOk.click();
        waitUntilElementClickable(navbarBrand);
    }
}
