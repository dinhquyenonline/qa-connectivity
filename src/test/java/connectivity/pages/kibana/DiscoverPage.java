package connectivity.pages.kibana;

import connectivity.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DiscoverPage extends BasePage {

    @FindBy(xpath = "//textarea[@placeholder='Search']")
    WebElement txtSearch;

    @FindBy(xpath = "//button[@data-test-subj='querySubmitButton']")
    WebElement btnRefresh;

    @FindBy(xpath = "//button[@aria-label='Toggle row details']")
    WebElement btnToggleRow;

    @FindBy(xpath = "//td[div//span[.='entity_json'] and contains(@class,'kbnDocViewer')]/following-sibling::td//button[@aria-label='Toggle field details']")
    WebElement btnToggleEntityField;

    @FindBy(xpath = "//td[div//span[.='entity_json'] and contains(@class,'kbnDocViewer')]/following-sibling::td//div")
    WebElement lblEntityJsonValue;

    public void search(String query){
        waitUntilProgressbarLoaded();
        enterValue(txtSearch, query);
        click(btnRefresh);
        waitUntilElementClickable(btnToggleRow);
    }

    public void clickFirstSearchResultRow(){
        click(btnToggleRow);
    }

    public void clickEntityFieldDetails(){
        click(btnToggleEntityField);
    }

    public String getEntityJsonValue(){
        scrollToElement(lblEntityJsonValue);
        return getText(lblEntityJsonValue);
    }
}
