package connectivity.utils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class JsonConfigurationReader {
    private static final String SUPPLIER_CONFIGURATION = "src/test/resources/schema/json/supplierconfigs.json";
    private static final String KIBANA_QUERY_LANGUAGE = "src/test/resources/schema/json/kibanaquery.json";

    public static Map<String, String> getSupplierConfigsByName(String supplerName) {
        JSONParser parser = new JSONParser();
        Map<String, String> configs = new HashMap<>();
        try {
            Object obj = parser.parse(new FileReader(SUPPLIER_CONFIGURATION));
            JSONArray jsonArray = (JSONArray) obj;
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                JSONObject subObjects = (JSONObject) jsonObject.get(supplerName);
                Set<String> keys = subObjects.keySet();
                for (String key : keys) {
                    String jsonKey = key;
                    String jsonValue = subObjects.get(key).toString();
                    configs.put(jsonKey, jsonValue);
                }
            }
        } catch (Throwable e) {
            e.getMessage();
        }
        return configs;
    }

    public static Map<String, String> getKibanaQueryByName(String supplerName) {
        JSONParser parser = new JSONParser();
        Map<String, String> configs = new HashMap<>();
        try {
            Object obj = parser.parse(new FileReader(KIBANA_QUERY_LANGUAGE));
            JSONArray jsonArray = (JSONArray) obj;
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                JSONObject subObjects = (JSONObject) jsonObject.get(supplerName);
                Set<String> keys = subObjects.keySet();
                for (String key : keys) {
                    String jsonKey = key;
                    String jsonValue = subObjects.get(key).toString();
                    configs.put(jsonKey, jsonValue);
                }
            }
        } catch (Throwable e) {
            e.getMessage();
        }
        return configs;
    }

    public static void main(String args[]) throws Throwable {
        JsonConfigurationReader jsonDataReader = new JsonConfigurationReader();
        jsonDataReader.getSupplierConfigsByName("SkyDeck").forEach((k, v) -> {
            System.out.println(k + " - " + v);
        });
    }
}
