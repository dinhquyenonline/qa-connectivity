package connectivity.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Timer {

    public static String getCurrentDateTime() {
        return LocalDateTime.now()
                .format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

    public static String getCurrentTimePlusMonth(int month) {
        return LocalDateTime.now().plusMonths(month)
                .format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }
}
