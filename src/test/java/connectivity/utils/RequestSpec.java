package connectivity.utils;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;

public class RequestSpec {
  /**
   * Get Request Specification for blog endpoint
   *
   * @return RequestSpecification
   */
  public static RequestSpecification jsonReqSpec() {
    EnvironmentVariables environmentVariables = Injectors.getInjector()
        .getInstance(EnvironmentVariables.class);

    String baseUrl = EnvironmentSpecificConfiguration.from(environmentVariables)
        .getProperty("baseurl");

    return new RequestSpecBuilder().setBaseUri(baseUrl)
        .setContentType("application/json")
        .build();
  }

  public static RequestSpecification xmlReqSpec() {
    EnvironmentVariables environmentVariables = Injectors.getInjector()
            .getInstance(EnvironmentVariables.class);

    String baseUrl = EnvironmentSpecificConfiguration.from(environmentVariables)
            .getProperty("baseurl");

    return new RequestSpecBuilder().setBaseUri(baseUrl)
            .setContentType("application/xml")
            .build();
  }
}
