package common.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Random {
    //Get random alphabetic characters
    public static String getRandomAlphaString(int length)  {
        try {
            String charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            charset = RandomStringUtils.random(length, charset.toCharArray());
            return charset;
        }catch(Exception ex) {
            ex.getMessage();
        }
        return null;
    }

    //Get random numeric characters
    public static String getRandomNumericString(int length)  {
        try {
            String charset = "1234567890";
            charset = RandomStringUtils.random(length, charset.toCharArray());
            return charset;
        }catch(Exception ex){
            ex.getMessage();
        }
        return null;
    }
}
