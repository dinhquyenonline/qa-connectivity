package suite.commonsteps;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.assertj.core.api.Assertions.assertThat;

import connectivity.business.ws.ews.EWSActions;
import connectivity.utils.RequestSpec;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import java.util.Map;

public class CommonSteps {
  EWSActions ewsActions = new EWSActions();

  @Step("Verify that API response is {0}")
  public void responseCodeIs(int responseCode, Response lastResponse) {
    assertThat(lastResponse.statusCode()).isEqualTo(responseCode);
  }

  @Step("Verify that response is empty list")
  public void responseShouldBeEmptyList(Response lastResponse) {
    assertThat(lastResponse.getBody().jsonPath().getList("").size()).isEqualTo(0);
  }

  @Step("Verify response schema with {1}")
  public void verifyResponseSchema(Response lastResponse, String schemaJson) {
    lastResponse.then().body(matchesJsonSchemaInClasspath("schema/" + schemaJson));
  }

  @Step("Call Tour List API")
  public Response callTourListBySupplierId(String supplierId) {
    return SerenityRest.given().spec(RequestSpec.xmlReqSpec())
            .body(ewsActions.createTourListRequestBySupplierId(supplierId))
            .post().then().extract().response();
  }

  @Step("Call Batch Availability API")
  public Response callBatchAvailabilityBySupplierId(Map<String, String> mods) {
    return SerenityRest.given().spec(RequestSpec.xmlReqSpec())
            .body(ewsActions.createBatchAvailabilityRequest(mods))
            .post().then().extract().response();
  }

  @Step("Call Availability API")
  public Response callAvailabilitySupplierId(Map<String, String> mod) {
    return SerenityRest.given().spec(RequestSpec.xmlReqSpec())
            .body(ewsActions.createAvailabilityRequest(mod))
            .post().then().extract().response();
  }

  @Step("Call Booking API")
  public Response callBookingSupplierId(Map<String, String> mod) {
    return SerenityRest.given().spec(RequestSpec.xmlReqSpec())
            .body(ewsActions.createBookingRequest(mod))
            .post().then().extract().response();
  }

  @Step("Call Batch Pricing API")
  public Response callBatchPricingBySupplierId(Map<String, String> mods) {
    return SerenityRest.given().spec(RequestSpec.xmlReqSpec())
            .body(ewsActions.createBatchPricingRequest(mods))
            .post().then().extract().response();
  }
}
