package suite.stepdefs;

import connectivity.pages.kibana.DiscoverPage;
import connectivity.pages.login.LoginPage;
import connectivity.utils.EnvConfigs;
import connectivity.utils.JsonConfigurationReader;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class KibanaSteps {
  LoginPage loginPage = new LoginPage();
  DiscoverPage discoverPage = new DiscoverPage();

  @Then("I login kibana page")
  public void login_kibana_page() {
    loginPage.openLoginPage(EnvConfigs.getEnvironmentConfigs().getProperty("kibanaUrl"));
    loginPage.login("readonly","readonly");
  }

  @Then("I filter {string} connector data in kibana")
  public  void i_filter_connector_in_kibana(String connectorName) {
    Map<String, String> queryLanguage  = JsonConfigurationReader.getKibanaQueryByName(connectorName);
    discoverPage.search(queryLanguage.get("products"));
  }

  @And("I validate city code {string} and country code {string} in kibana")
  public  void i_validate_city_country_in_kibana(String cityCode, String countryCode) {
    discoverPage.clickFirstSearchResultRow();
    discoverPage.clickEntityFieldDetails();
    assertThat(discoverPage.getEntityJsonValue().contains("\"city\\\\\\\":null"));
    assertThat(discoverPage.getEntityJsonValue().contains("\"country\\\\\\\":null"));
  }

}
