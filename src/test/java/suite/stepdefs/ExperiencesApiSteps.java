package suite.stepdefs;

import connectivity.business.datacontext.TestContext;
import connectivity.utils.Context;
import connectivity.utils.Timer;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import suite.commonsteps.CommonSteps;
import java.util.HashMap;
import java.util.Map;
import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static org.assertj.core.api.Assertions.assertThat;

public class ExperiencesApiSteps {
    @Steps
    CommonSteps commonSteps;

    @When("I call the tour list with supplier Id {string}")
    public void i_call_the_tour_list_with_supplier_id(String supplierId) {
        commonSteps.callTourListBySupplierId(supplierId);
    }

    @When("I call the Batch Availability with supplier id {string}")
    public void i_call_the_batch_availability_with_supplier_id(String supplierId) {
        Map<String, String> mods = new HashMap();
        mods.put("SupplierId", supplierId);
        mods.put("StartDate", Timer.getCurrentDateTime());
        mods.put("EndDate", Timer.getCurrentTimePlusMonth(12));
        commonSteps.callBatchAvailabilityBySupplierId(mods);

        //Set data for availability called
        TestContext.getScenarioContext().setContext(Context.START_DATE, lastResponse().getBody().xmlPath().
                getString("BatchAvailabilityResponse.BatchTourAvailability.find { it.AvailabilityStatus.Status == 'AVAILABLE' }.Date[0]"));
        TestContext.getScenarioContext().setContext(Context.SUPPLIER_PRODUCT_CODE, lastResponse().getBody().xmlPath().
                getString("BatchAvailabilityResponse.BatchTourAvailability.find { it.AvailabilityStatus.Status == 'AVAILABLE' }.SupplierProductCode[0]"));
    }

    @When("I call the availability with supplier id {string}")
    public void i_call_the_availability_with_supplier_id(String supplierId) {
        Map<String, String> mods = new HashMap();
        mods.put("SupplierId",
                supplierId);
        mods.put("StartDate",
                TestContext.getScenarioContext().getContext(Context.START_DATE).toString());
        mods.put("SupplierProductCode",
                TestContext.getScenarioContext().getContext(Context.SUPPLIER_PRODUCT_CODE).toString());
        commonSteps.callAvailabilitySupplierId(mods);
    }


    @When("I call the booking with supplier id {string}")
    public void i_call_the_booking_with_supplier_id(String supplierId) {
        Map<String, String> mods = new HashMap();
        mods.put("SupplierId",
                supplierId);
        mods.put("TravelDate",
                TestContext.getScenarioContext().getContext(Context.START_DATE).toString());
        mods.put("SupplierProductCode",
                TestContext.getScenarioContext().getContext(Context.SUPPLIER_PRODUCT_CODE).toString());
        commonSteps.callBookingSupplierId(mods);
    }

    @Then("I verify response code")
    public void verify_response_code() {
        commonSteps.responseCodeIs(200,
                lastResponse());
    }

    @Then("I verify tour list response message")
    public void verify_response_msg() {
        assertThat(lastResponse().getBody().xmlPath().
                getString("TourListResponse.RequestStatus.Status")).isEqualTo("SUCCESS");
    }

    @Then("I verify batch availability response message")
    public void verify_batch_availability_response_msg() {
        assertThat(lastResponse().getBody().xmlPath().
                getString("BatchAvailabilityResponse.RequestStatus.Status")).isEqualTo("SUCCESS");
    }

    @Then("I verify availability response message")
    public void verify_availability_response_msg() {
        assertThat(lastResponse().getBody().xmlPath().
                getString("AvailabilityResponse .RequestStatus.Status")).isEqualTo("SUCCESS");
    }

    @Then("I verify booking response message")
    public void i_verify_booking_response_message() {
        assertThat(lastResponse().getBody().xmlPath().
                getString("BookingResponse .RequestStatus.Status")).isEqualTo("SUCCESS");
        assertThat(lastResponse().getBody().xmlPath().
                getString("BookingResponse .TransactionStatus.Status")).isEqualTo("CONFIRMED");
    }

    @When("I call the batch pricing with supplier id {string}")
    public void i_call_the_batch_pricing_with_supplier_id(String supplierId) {
        Map<String, String> mods = new HashMap();
        mods.put("SupplierId", supplierId);
        mods.put("StartDate", Timer.getCurrentDateTime());
        mods.put("EndDate", Timer.getCurrentTimePlusMonth(12));
        commonSteps.callBatchPricingBySupplierId(mods);
    }

    @Then("I verify batch pricing response message")
    public void verify_batch_pricing_response_msg() {
        assertThat(lastResponse().getBody().xmlPath().
                getString("BatchPricingResponse.RequestStatus.Status")).isEqualTo("SUCCESS");
    }
}
