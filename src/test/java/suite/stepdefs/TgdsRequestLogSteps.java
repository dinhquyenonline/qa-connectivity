package suite.stepdefs;

import connectivity.pages.tgdslog.RequestLogPage;
import connectivity.utils.EnvConfigs;
import cucumber.api.java.en.Then;

public class TgdsRequestLogSteps {
    RequestLogPage requestLogPage = new RequestLogPage();

    @Then("I open the rml log page")
    public void i_open_the_rml_log_page() {
        requestLogPage.openTgdsRequestLog(EnvConfigs.getEnvironmentConfigs().getProperty("rmlLogUrl"));
    }

    @Then("I select supplier and api type {string}")
    public void open_the_tgds_admin_page(String apiType) {
        requestLogPage.selectSupplier("Historic Royal Palaces [3251] [20180501]");
        requestLogPage.selectApiType(apiType);
        requestLogPage.clickSubmitBtn();
    }

    @Then("I select first booking request")
    public void select_first_booking_request() {
        requestLogPage.openRequestLogByIndex(1);
    }
}
