package suite.stepdefs;

import connectivity.pages.login.LoginPage;
import connectivity.pages.tgdsadmin.SupplierConfigurationPage;
import connectivity.pages.tgdsadmin.TgdsAdminHomePage;
import connectivity.utils.EnvConfigs;
import connectivity.utils.JsonConfigurationReader;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import suite.commonsteps.CommonSteps;

import java.util.AbstractMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class TgdsAdminSteps {
  LoginPage loginPage = new LoginPage();
  TgdsAdminHomePage tgdsAdminHomePage = new TgdsAdminHomePage();
  SupplierConfigurationPage supplierConfigurationPage = new SupplierConfigurationPage();

  @Steps
  CommonSteps commonSteps;

  @Steps
  ExperiencesApiSteps experiencesApiSteps;

  @When("I open the tgds admin page")
  public void open_the_tgds_admin_page() {
    loginPage.openLoginPage(EnvConfigs.getEnvironmentConfigs().getProperty("tgdsAdminUrl"));
  }

  @Then("I login to TGDS Admin page")
  public void input_username_and_password(){
    loginPage.login("qvu","123456789@Qt");
  }

  @Then("I verify that login successfully")
  public void verify_the_result(){
    assertThat(tgdsAdminHomePage.isHomePageDisplayed()).isEqualTo(true);
  }

  @Then("I select the supplier by Id {string}")
  public void i_search_supplier_by_Id(String supplierId) {
    tgdsAdminHomePage.findSupplier(new AbstractMap.SimpleEntry<>("Supplier Id or Name", supplierId));
  }

  @Then("I update the configuration for {string} supplier")
  public void update_supplier_configuration(String supplierName){
    supplierConfigurationPage.clickEditBtn();

    Map<String, String> configs  = JsonConfigurationReader.getSupplierConfigsByName(supplierName);
    supplierConfigurationPage.changeSupplierConfigurations(configs);
  }

  @Then("I book the tour with supplier id {string}")
  public void book_tour(String supplierId){
    experiencesApiSteps.i_call_the_batch_availability_with_supplier_id(supplierId);
    experiencesApiSteps.i_call_the_booking_with_supplier_id(supplierId);
  }
}
