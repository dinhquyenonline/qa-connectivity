package suite.stepdefs;

import common.wdm.WDFactory;
import common.wdm.WdManager;
import connectivity.utils.EnvConfigs;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;
import java.util.Map;

public class Hooks {
    @Before("@UI")
    public void before() throws Exception {
        String browserType = StringUtils.isEmpty(System.getProperty("browserName")) ? EnvConfigs.getEnvironmentConfigs().getProperty("browserType") : System.getProperty("browserName");
        String serverType = StringUtils.isEmpty(System.getProperty("server")) ? EnvConfigs.getEnvironmentConfigs().getProperty("runType") : System.getProperty("server");
        String remoteUrl = StringUtils.isEmpty(System.getProperty("remoteUrl")) ? EnvConfigs.getEnvironmentConfigs().getProperty("remoteUrl") : System.getProperty("remoteUrl");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
//        capabilities.setCapability("selenoid:options", Map.<String, Object>of(
//                "enableVNC", true,
//                "enableVideo", true
//        ));
        switch (browserType) {
            case "Chrome":
            case "chrome":
            case "gc":
                if (serverType.equalsIgnoreCase("remote")) {
                    capabilities.setCapability("browserName", "chrome");
                    WdManager.set(WDFactory.remote(new URL(remoteUrl), capabilities));
                } else {
                    ChromeOptions options = new ChromeOptions();
                    if (serverType.equalsIgnoreCase("headless")) {
                        options.setHeadless(true);
                    }
                    options.setAcceptInsecureCerts(true);
                    WdManager.set(WDFactory.initBrowser("gc", options));
                }
                break;
            case "Firefox":
            case "firefox":
            case "ff":
                if (serverType.equalsIgnoreCase("remote")) {
                    capabilities.setCapability("browserName", "firefox");
                    WdManager.set(WDFactory.remote(new URL(remoteUrl), capabilities));
                } else {
                    FirefoxBinary firefoxBinary = new FirefoxBinary();
                    FirefoxOptions options = new FirefoxOptions();
                    options.setBinary(firefoxBinary);
                    if (serverType.equalsIgnoreCase("headless")) {
                        options.setHeadless(true);
                    }
                    options.setAcceptInsecureCerts(true);
                    WdManager.set(WDFactory.initBrowser("ff", options));
                }
                break;
            case "ie":
                WdManager.set(WDFactory.remote(new URL(remoteUrl), capabilities));
                break;
            default:
                throw new Exception("Please check the browser configuration");
        }
        WdManager.get().manage().window().maximize();
    }

    @After("@UI")
    public void after() {
        WdManager.dismissWD();
    }
}
