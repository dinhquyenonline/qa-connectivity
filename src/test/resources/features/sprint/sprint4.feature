@Sprint4
Feature: Sprint 4
  I should be able to ensure the quality of Sprint 4

  @UI @SUPPLY-4907
  Scenario: SUPPLY-4907 - Tour List - Batch pricing
#    When I call the tour list with supplier Id "20200311"
#    Then I verify response code
#    Then I verify tour list response message
#    When I call the batch pricing with supplier id "20200311"
#    Then I verify response code
#    Then I verify batch pricing response message
    Then I login kibana page
    Then I filter "Encore" connector data in kibana
    And I validate city code "LDN" and country code "UK" in kibana


