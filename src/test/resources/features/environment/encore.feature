@Encore
Feature: Encore
  As an AM user
  I should be able to test Encore Connector

  @Non-UI
  Scenario: Tour List API
    When I call the tour list with supplier Id "20200311"
    Then I verify response code
    Then I verify tour list response message

  @Non-UI
  Scenario: Batch Availability API
    When I call the Batch Availability with supplier id "20200311"
    Then I verify response code
    Then I verify batch availability response message

  @Non-UI
  Scenario: Availability API
    When I call the availability with supplier id "20200311"
    Then I verify response code
    Then I verify availability response message

  @Non-UI
  Scenario: Booking API
    When I call the booking with supplier id "20200311"
    Then I verify response code
    Then I verify booking response message



