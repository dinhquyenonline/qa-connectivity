@Bos
Feature: BOS V2
  I should be able to test BOS V2 Connector

  @Non-UI
  Scenario: Tour List API - Historic Royal Palaces
    When I call the tour list with supplier Id "20180501"
    Then I verify response code
    Then I verify tour list response message

  @Non-UI
  Scenario: Batch Availability API - Historic Royal Palaces
    When I call the Batch Availability with supplier id "20180501"
    Then I verify response code
    Then I verify batch availability response message

  @Non-UI
  Scenario: Availability API - Historic Royal Palaces
    When I call the availability with supplier id "20180501"
    Then I verify response code
    Then I verify availability response message

  @Non-UI
  Scenario: Booking API - Historic Royal Palaces
    When I call the booking with supplier id "20180501"
    Then I verify response code
    Then I verify booking response message



